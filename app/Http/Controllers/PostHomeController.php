<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostHomeController extends Controller
{
    //
    /**
     * Show the posts
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function posts()
    {
      $posts = Post::all();
        return view('posts',['posts' => $posts]);
    }
    public function json()
    {
      $post = Post::with('comments')->get();
      return $post;
    }
}
