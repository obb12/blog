<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;

class CommentController extends Controller
{
    //
    public function comment(Request $request)
    {
      if ($request->has('post_id')&&$request->has('content')) {
        $new = new Comment;
        $new->content  = $request->content;
        $new->post_id = $request->post_id;
        $new->save();
      return  redirect('/posts');
      }
    }
}
