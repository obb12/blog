
@extends('layouts.app')

@section('content')
@foreach ($posts as $post)

<div class="card ">
  <div class="card-body">
      <h5 class="card-title">{{ $post->title }}</h5>
      <p class="card-text">
        {!! htmlspecialchars_decode(GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($post->content), ENT_NOQUOTES) !!}
        </p>
        {{$post->created_at->diffForHumans()}}

    </div>
    <div class="card-footer">

      <form class="" action="/comment" method="post">
@csrf
<input  type="hidden" name="post_id" value="{{$post->id}}">
        <textarea class="form-control" name="content"></textarea>
        <button class="btn btn-primary btn-lg btn-block" type="submit" ><i class="fas fa-comment"></i></button>
      </form>
      @foreach($post->comments as $comment)
      <div class="card ">
        <div class="card-body">
          <p class="card-text">
            {!! htmlspecialchars_decode(GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($comment->content), ENT_NOQUOTES) !!}

    </p>
  </div>
      <div class="card-footer">
        <p>
          {{$comment->created_at->diffForHumans()}}

        </p>
      </div>
        </div>
      @endforeach
      </div>
</div>

@endforeach
@endsection
